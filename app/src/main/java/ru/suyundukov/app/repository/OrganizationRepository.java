package ru.suyundukov.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.suyundukov.app.entity.Organization;
@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Integer> {
    Organization findOrganizationById(Integer id) throws Exception;
}
