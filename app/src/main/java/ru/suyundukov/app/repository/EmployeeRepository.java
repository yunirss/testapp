package ru.suyundukov.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.suyundukov.app.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee , Integer> {
    Employee findEmployeeById(Integer id) throws Exception;
}
