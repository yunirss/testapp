package ru.suyundukov.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.suyundukov.app.entity.Order;
import ru.suyundukov.app.exception_handling.NoSuchEntityException;
import ru.suyundukov.app.service.OrderService;

import java.util.List;

@RestController
@RequestMapping(value = "/order")
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> save(@RequestBody Order order) {
        orderService.save(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Order>> findAll() throws Exception {
        List<Order> orders = orderService.findAll();
        if (orders.isEmpty()) {
            throw new NoSuchEntityException("There is no orders");
        }
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping(value = "/getOrders/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<Order> getOrders(@RequestBody Order order,
                                 @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return orderService.getOrders(order, pageable);
    }

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> findOrderById(@PathVariable Integer id) throws Exception {
        Order order = orderService.findById(id);
        if (order == null) {
            throw new NoSuchEntityException("There is no order with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> update(@RequestBody Order order) throws Exception {
        orderService.save(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) throws Exception {
        orderService.deleteById(id);
        return "Order with Id = " + id + " was deleted";
    }
}