package ru.suyundukov.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.suyundukov.app.entity.Department;
import ru.suyundukov.app.exception_handling.NoSuchEntityException;
import ru.suyundukov.app.service.DepartmentService;

import java.util.List;

@RestController
@RequestMapping(value = "/department")
public class DepartmentController {
    private final DepartmentService departmentService;

    @Autowired
    public DepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Department> save(@RequestBody Department department) {
        departmentService.save(department);
        return new ResponseEntity<>(department, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Department>> findAll() throws Exception {
        List<Department> departments = departmentService.findAll();
        if (departments.isEmpty()) {
            throw new NoSuchEntityException("There is no departments");
        }
        return new ResponseEntity<>(departments, HttpStatus.OK);
    }

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Department> findDepartmentById(@PathVariable Integer id) throws Exception {
        Department department = departmentService.findById(id);
        if (department == null) {
            throw new NoSuchEntityException("There is no department with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(department, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Department> update(@RequestBody Department department) throws Exception {
        departmentService.save(department);
        return new ResponseEntity<>(department, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) throws Exception {
        departmentService.deleteById(id);
        return "Department with Id = " + id + " was deleted";
    }
}
