package ru.suyundukov.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.suyundukov.app.entity.Employee;
import ru.suyundukov.app.exception_handling.NoSuchEntityException;
import ru.suyundukov.app.service.EmployeeService;

import java.util.List;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {
    @Autowired
    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping(value = "/post", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> save(@RequestBody Employee employee) {
        try {
            employeeService.save(employee);
        } catch (Exception e) {
            throw new NoSuchEntityException("Employee is not saved");
        }
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> findAll()  {
        List<Employee> employees;
        try {
            employees = employeeService.findAll();
        } catch (Exception e) {
            throw new NoSuchEntityException("There is no employees");
        }
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }


    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> findEmployeeById(@PathVariable Integer id) throws Exception {
        Employee employee = employeeService.findById(id);
        if (employee == null) {
            throw new NoSuchEntityException("There is no employee with ID = " + id + " in database!");
        }
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> update(@RequestBody Employee employee) {
        try {
            employeeService.save(employee);
        } catch (Exception e) {
            throw new NoSuchEntityException("Employee is not updated");
        }
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) throws Exception {
        employeeService.deleteById(id);
        return "Employee with Id = " + id + " was deleted";
    }
}
