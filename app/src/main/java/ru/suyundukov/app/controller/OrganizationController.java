package ru.suyundukov.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.suyundukov.app.entity.Organization;
import ru.suyundukov.app.exception_handling.NoSuchEntityException;
import ru.suyundukov.app.service.OrganizationService;

import java.util.List;

@RestController
@RequestMapping(value = "/organization")
public class OrganizationController {
    private final OrganizationService organizationService;

    public OrganizationController(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

    @PostMapping(value = "/post", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Organization> save(@RequestBody Organization organization){
        organizationService.save(organization);
        return new ResponseEntity<>(organization, HttpStatus.OK);
    }

    @GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Organization>> findAll() throws Exception {
        List<Organization> organizations = organizationService.findAll();
        if (organizations.isEmpty()) {
            throw new NoSuchEntityException("There is no organizations");
        }
        return new ResponseEntity<>(organizations, HttpStatus.OK);
    }

    @GetMapping(value = "/getById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Organization> findOrganizationById(@PathVariable Integer id) throws Exception {
        Organization organization = organizationService.findById(id);
        if (organization == null) {
            throw new NoSuchEntityException("There is no organizations with ID = " + id + " in Database!");
        }
        return new ResponseEntity<>(organization, HttpStatus.OK);
    }
    @PutMapping(value = "/put", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Organization> update(@RequestBody Organization organization) throws Exception {
        organizationService.save(organization);
        return new ResponseEntity<>(organization, HttpStatus.OK);
    }

    @DeleteMapping(value = "/deleteById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteById(@PathVariable Integer id) throws Exception {
        organizationService.deleteById(id);
        return "Organization with Id = " + id + " was deleted";
    }
}
