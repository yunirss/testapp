package ru.suyundukov.app.service;

import ru.suyundukov.app.entity.Organization;

public interface OrganizationService extends AbstractService<Organization>{
}
