package ru.suyundukov.app.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.suyundukov.app.entity.Organization;
import ru.suyundukov.app.repository.OrganizationRepository;
import ru.suyundukov.app.service.OrganizationService;

import java.util.List;

@Service
public class OrganizationServiceImpl implements OrganizationService {
    private final OrganizationRepository organizationRepository;

    public OrganizationServiceImpl(OrganizationRepository organizationRepository) {
        this.organizationRepository = organizationRepository;
    }

    @Override
    @Transactional
    public void save(Organization organization)   {
        organizationRepository.save(organization);
    }

    @Override
    public Organization findById(Integer id) throws Exception {
        return organizationRepository.findOrganizationById(id);
    }

    @Override
    public List<Organization> findAll() throws Exception {
        return organizationRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteById(Integer id) throws Exception {
        organizationRepository.deleteById(id);
    }
}
