package ru.suyundukov.app.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.suyundukov.app.entity.Order;

public interface OrderService extends AbstractService<Order>{
    Page<Order> getOrders(Order order, Pageable pageable);
}
