package ru.suyundukov.app.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.suyundukov.app.entity.Department;
import ru.suyundukov.app.repository.DepartmentRepository;
import ru.suyundukov.app.service.DepartmentService;

import java.util.List;
@Service
public class DepartmentServiceImpl implements DepartmentService {
    private final DepartmentRepository departmentRepository;

    public DepartmentServiceImpl(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public void save(Department department) {
        departmentRepository.save(department);
    }

    @Override
    public Department findById(Integer id) throws Exception {
        return departmentRepository.findDepartmentById(id);
    }

    @Override
    public List<Department> findAll() throws Exception {
        return departmentRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteById(Integer id) throws Exception {
        departmentRepository.deleteById(id);
    }
}
