package ru.suyundukov.app.service;

import java.util.List;

public interface AbstractService<E> {
    void save(E e);

    E findById(Integer id) throws Exception;

    List<E> findAll() throws Exception;

    void deleteById(Integer id) throws Exception;
}