package ru.suyundukov.app.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.suyundukov.app.entity.Order;
import ru.suyundukov.app.repository.OrderRepository;
import ru.suyundukov.app.service.OrderService;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    @Transactional
    public void save(Order order)   {
        orderRepository.save(order);
    }

    @Override
    public Order findById(Integer id) throws Exception {
        return orderRepository.findOrderById(id);
    }

    @Override
    public List<Order> findAll() throws Exception {
        return orderRepository.findAll();
    }

    @Override
    @Transactional
    public void deleteById(Integer id) throws Exception {
        orderRepository.deleteById(id);
    }

    @Override
    public Page<Order> getOrders(Order order, Pageable pageable) {
        return orderRepository.findAll(pageable);
    }
}
