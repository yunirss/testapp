package ru.suyundukov.app.service;

import ru.suyundukov.app.entity.Department;

public interface DepartmentService extends AbstractService<Department> {

}
