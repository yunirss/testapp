package ru.suyundukov.app.service;

import ru.suyundukov.app.entity.Employee;


public interface EmployeeService extends AbstractService<Employee> {

}
