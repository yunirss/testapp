insert into 'departments' ( contact_info, name)
values ('Ufa ',  'IT');

insert into 'orders' (control, execution,text)
values (true, true,  'text ');

insert into 'organizations' (legal_address, name, physical_address)
values ('Pavlov street ',  'IT-Company ',  'Ivanova street ');

insert into 'employees' (employee_id,  first_name, last_name, patronymic)
values ('Ivan ',  'Ivanov ',  'Ivanovich');